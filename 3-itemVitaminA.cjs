const data = require("./3-arrays-vitamins.cjs")

function allVitaminAItem(data) {
    let result = data.filter((item) => {
        return item.contains.includes("Vitamin A");
    })
    return result;
}
const result = allVitaminAItem(data);
console.log(result);
