const data = require("./3-arrays-vitamins.cjs")

function groupItem(data) {
    const resultData = data.reduce((previous, current) => {
        let splitData = current.contains.split(",").map((item) => {
            if (previous[item.trim()] === undefined) {
                previous[item] = [];
            }
            previous[item.trim()].push(current.name);
        })
        return previous;
    }, {});
    return resultData;
}
const result = groupItem(data);
console.log(result);