const data = require("./3-arrays-vitamins.cjs")

function allAvailableItem(data) {
    let result = data.filter((item) => {
        return item.available === true;
    })
    return result;
}
const result = allAvailableItem(data);
console.log(result);
