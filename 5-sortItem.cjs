const data = require("./3-arrays-vitamins.cjs")

function numberOfVitamin(data) {
    let result = data.sort((item1, item2) => {
        return item2.contains.split(",").length - item1.contains.split(",").length
    })
    return result;
}
const result = numberOfVitamin(data);
console.log(result);
